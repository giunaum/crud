package br.com.crud.dto.mapper;

import br.com.crud.document.Cliente;
import br.com.crud.dto.ClienteUpdateDTO;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;

@AllArgsConstructor
@Component
public class ClienteUpdateMapper implements InterfaceMapper<Cliente, ClienteUpdateDTO> {

	@Override
	public Cliente toEntity(ClienteUpdateDTO dto) {
		return Cliente.builder()
				.id(Optional.ofNullable(dto).map(ClienteUpdateDTO::getId).orElse(null))
				.nome(Optional.ofNullable(dto).map(ClienteUpdateDTO::getNome).orElse(null))
				.dataNascimento(Optional.ofNullable(dto).map(ClienteUpdateDTO::getDataNascimento).orElse(null))
				.build();
	}

	@Override
	public ClienteUpdateDTO toDTO(Cliente entity) {
		return ClienteUpdateDTO.builder()
				.id(Optional.ofNullable(entity).map(Cliente::getId).orElse(null))
				.nome(Optional.ofNullable(entity).map(Cliente::getNome).orElse(null))
				.dataNascimento(Optional.ofNullable(entity).map(Cliente::getDataNascimento).orElse(null))
				.build();
	}

	public Cliente toCliente(Cliente cliente, ClienteUpdateDTO clienteUpdate) {
		return Cliente.builder()
				.id(cliente.getId())
				.cpf(cliente.getCpf())
				.nome(Optional.ofNullable(clienteUpdate.getNome()).orElse(cliente.getNome()))
				.dataNascimento(Optional.ofNullable(clienteUpdate.getDataNascimento()).orElse(cliente.getDataNascimento()))
				.build();
	}
}
