package br.com.crud.repository;

import br.com.crud.document.Cliente;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

public interface ClienteRepository extends MongoRepository<Cliente, String> {

	@Query(value = "{'$or' : [{'nome': {$regex : ?0, $options: 'i'}}, {'cpf': {$regex : ?0, $options: 'i'}}]}")
	Page<Cliente> findByNomeOrCpf(String parametro, Pageable pageable);
}
