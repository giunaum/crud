package br.com.crud.util;

import org.apache.commons.lang3.StringUtils;

import java.math.BigInteger;
import java.text.MessageFormat;

public class Utilitario {

	private Utilitario() {
		throw new IllegalStateException("Classe utilitária! Não pode ser instanciada.");
	}

	/**
	 * Formata a {@link String} conforme os parâmetros fornecidos.
	 *
	 * @param isConcatenar
	 * @param entrada
	 * @param parametros
	 * @return
	 */
	public static String formatarString(boolean isConcatenar, String entrada, String... parametros) {
		String stringFormatada = entrada;

		if (StringUtils.isNotBlank(entrada)) {
			if (isConcatenar) {
				stringFormatada = getStringComParametrosConcatenados(stringFormatada, parametros);
			} else {
				stringFormatada = MessageFormat.format(stringFormatada, parametros);
			}
		}

		return stringFormatada;
	}

	/**
	 * Inclui os parâmetros Concatenados conforme os parâmetros fornecidos.
	 *
	 * @param entrada
	 * @param parametros
	 * @return
	 */
	private static String getStringComParametrosConcatenados(String entrada, String... parametros) {
		String stringFormatada = entrada;

		String parametrosConcatenados = getParametrosConcatenados(parametros);
		if (parametrosConcatenados.trim().length() > BigInteger.ZERO.intValue()) {
			stringFormatada = MessageFormat.format(stringFormatada, parametrosConcatenados);
		}
		return stringFormatada;
	}

	/**
	 * Retorna os parâmetros separados por virgula concatenados em uma {@link String}.
	 *
	 * @param parametros
	 * @return
	 */
	private static String getParametrosConcatenados(String... parametros) {
		StringBuilder parametrosConcatenados = new StringBuilder();

		boolean isVirgula = false;
		for (String parametro : parametros) {
			if (isVirgula) {
				parametrosConcatenados.append(", ");
			}

			parametrosConcatenados.append(parametro);
			isVirgula = true;
		}

		return parametrosConcatenados.toString();
	}
}
