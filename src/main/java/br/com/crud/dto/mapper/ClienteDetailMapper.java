package br.com.crud.dto.mapper;

import br.com.crud.document.Cliente;
import br.com.crud.dto.ClienteDetailDTO;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.math.BigInteger;
import java.time.LocalDate;
import java.time.Period;
import java.util.Optional;

@AllArgsConstructor
@Component
public class ClienteDetailMapper implements InterfaceMapper<Cliente, ClienteDetailDTO> {

	@Override
	public Cliente toEntity(ClienteDetailDTO dto) {
		return Cliente.builder()
				.id(Optional.ofNullable(dto).map(ClienteDetailDTO::getId).orElse(null))
				.nome(Optional.ofNullable(dto).map(ClienteDetailDTO::getNome).orElse(null))
				.cpf(Optional.ofNullable(dto).map(ClienteDetailDTO::getCpf).orElse(null))
				.dataNascimento(Optional.ofNullable(dto).map(ClienteDetailDTO::getDataNascimento).orElse(null))
				.build();
	}

	@Override
	public ClienteDetailDTO toDTO(Cliente entity) {
		return ClienteDetailDTO.builder()
				.id(Optional.ofNullable(entity).map(Cliente::getId).orElse(null))
				.nome(Optional.ofNullable(entity).map(Cliente::getNome).orElse(null))
				.cpf(Optional.ofNullable(entity).map(Cliente::getCpf).orElse(null))
				.dataNascimento(Optional.ofNullable(entity).map(Cliente::getDataNascimento).orElse(null))
				.idade(Optional.ofNullable(entity).map(Cliente::getDataNascimento)
						.map(this::getIdade).orElse(BigInteger.ZERO.intValue()))
				.build();
	}

	private int getIdade(LocalDate dataNascimento) {
		return Optional.ofNullable(dataNascimento)
				.map(dtNasc -> Period.between(dtNasc, LocalDate.now()))
				.map(Period::getYears).orElse(BigInteger.ZERO.intValue());
	}
}
