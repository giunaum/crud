package br.com.crud.dto.mapper;

import br.com.crud.document.Cliente;
import br.com.crud.dto.ClienteDTO;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;

@AllArgsConstructor
@Component
public class ClienteMapper implements InterfaceMapper<Cliente, ClienteDTO> {

	@Override
	public Cliente toEntity(ClienteDTO dto) {
		return Cliente.builder()
				.id(Optional.ofNullable(dto).map(ClienteDTO::getId).orElse(null))
				.nome(Optional.ofNullable(dto).map(ClienteDTO::getNome).orElse(null))
				.cpf(Optional.ofNullable(dto).map(ClienteDTO::getCpf).orElse(null))
				.dataNascimento(Optional.ofNullable(dto).map(ClienteDTO::getDataNascimento).orElse(null))
				.build();
	}

	@Override
	public ClienteDTO toDTO(Cliente entity) {
		return ClienteDTO.builder()
				.id(Optional.ofNullable(entity).map(Cliente::getId).orElse(null))
				.nome(Optional.ofNullable(entity).map(Cliente::getNome).orElse(null))
				.cpf(Optional.ofNullable(entity).map(Cliente::getCpf).orElse(null))
				.dataNascimento(Optional.ofNullable(entity).map(Cliente::getDataNascimento).orElse(null))
				.build();
	}
}
