package br.com.crud.exception;

import br.com.crud.util.Message;
import br.com.crud.util.MessageCode;
import br.com.crud.util.Utilitario;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.List;

/**
 * Classe de exceção customizada.
 */
public class CustomException extends RuntimeException implements Serializable {

	/**
	 * Construtor da classe de exceção.
	 *
	 * @param message
	 * @param cause
	 */
	public CustomException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Construtor da classe de exceção.
	 *
	 * @param loggerClass
	 * @param message
	 * @param cause
	 */
	public CustomException(Class<?> loggerClass, String message, Throwable cause) {
		super(message, cause);
		logger(loggerClass, message, cause);
	}

	/**
	 * Construtor da classe de exceção.
	 *
	 * @param message
	 */
	public CustomException(String message) {
		super(message);
	}

	/**
	 * Construtor da classe de exceção.
	 *
	 * @param loggerClass
	 * @param message
	 */
	public CustomException(Class<?> loggerClass, String message) {
		super(message);
		logger(loggerClass, message);
	}

	/**
	 * Construtor da classe de exceção.
	 *
	 * @param code
	 */
	public CustomException(MessageCode code) {
		super(Message.get(code));
	}

	/**
	 * Construtor da classe de exceção.
	 *
	 * @param loggerClass
	 * @param code
	 */
	public CustomException(Class<?> loggerClass, MessageCode code) {
		super(Message.get(code));
		logger(loggerClass, code);
	}

	/**
	 * Construtor da classe de exceção.
	 *
	 * @param message
	 * @param parametros
	 */
	public CustomException(String message, List<String> parametros) {
		super(Utilitario.formatarString(Boolean.TRUE, message, parametros.toArray(new String[parametros.size()])));
	}

	/**
	 * Construtor da classe de exceção.
	 *
	 * @param loggerClass
	 * @param message
	 * @param parametros
	 */
	public CustomException(Class<?> loggerClass, String message, List<String> parametros) {
		super(Utilitario.formatarString(Boolean.TRUE, message, parametros.toArray(new String[parametros.size()])));
		logger(loggerClass, message, parametros);
	}

	/**
	 * Construtor da classe de exceção.
	 *
	 * @param message
	 * @param parametros
	 */
	public CustomException(String message, String... parametros) {
		super(Utilitario.formatarString(Boolean.TRUE, message, parametros));
	}

	/**
	 * Construtor da classe de exceção.
	 *
	 * @param loggerClass
	 * @param message
	 * @param parametros
	 */
	public CustomException(Class<?> loggerClass, String message, String... parametros) {
		super(Utilitario.formatarString(Boolean.TRUE, message, parametros));
		logger(loggerClass, message, parametros);
	}

	/**
	 * Construtor da classe de exceção.
	 *
	 * @param code
	 * @param parametros
	 */
	public CustomException(MessageCode code, List<String> parametros) {
		super(Utilitario.formatarString(Boolean.TRUE, Message.get(code), parametros.toArray(new String[parametros.size()])));
	}

	/**
	 * Construtor da classe de exceção.
	 *
	 * @param loggerClass
	 * @param code
	 * @param parametros
	 */
	public CustomException(Class<?> loggerClass, MessageCode code, List<String> parametros) {
		super(Utilitario.formatarString(Boolean.TRUE, Message.get(code), parametros.toArray(new String[parametros.size()])));
		logger(loggerClass, code, parametros);
	}

	/**
	 * Construtor da classe de exceção.
	 *
	 * @param code
	 * @param parametros
	 */
	public CustomException(MessageCode code, String... parametros) {
		super(Utilitario.formatarString(Boolean.TRUE, Message.get(code), parametros));
	}

	/**
	 * Construtor da classe de exceção.
	 *
	 * @param loggerClass
	 * @param code
	 * @param parametros
	 */
	public CustomException(Class<?> loggerClass, MessageCode code, String... parametros) {
		super(Utilitario.formatarString(Boolean.TRUE, Message.get(code), parametros));
		logger(loggerClass, code, parametros);
	}

	/**
	 * Insere a mensagem no logger.
	 *
	 * @param loggerClass
	 * @param message
	 */
	protected void logger(Class<?> loggerClass, String message) {
		Logger logger = LoggerFactory.getLogger(loggerClass);
		logger.warn(message);
	}

	/**
	 * Insere a mensagem no logger.
	 *
	 * @param loggerClass
	 * @param message
	 * @param parametros
	 */
	protected void logger(Class<?> loggerClass, String message, List<String> parametros) {
		logger(loggerClass, Utilitario.formatarString(Boolean.TRUE, message, parametros.toArray(new String[parametros.size()])));
	}

	/**
	 * Insere a mensagem no logger.
	 *
	 * @param loggerClass
	 * @param code
	 * @param parametros
	 */
	protected void logger(Class<?> loggerClass, MessageCode code, List<String> parametros) {
		logger(loggerClass, Utilitario.formatarString(Boolean.TRUE, Message.get(code), parametros.toArray(new String[parametros.size()])));
	}

	/**
	 * Insere a mensagem no logger.
	 *
	 * @param loggerClass
	 * @param message
	 * @param parametros
	 */
	protected void logger(Class<?> loggerClass, String message, String... parametros) {
		logger(loggerClass, Utilitario.formatarString(Boolean.TRUE, message, parametros));
	}

	/**
	 * Insere a mensagem no logger.
	 *
	 * @param loggerClass
	 * @param code
	 * @param parametros
	 */
	protected void logger(Class<?> loggerClass, MessageCode code, String... parametros) {
		logger(loggerClass, Utilitario.formatarString(Boolean.TRUE, Message.get(code), parametros));
	}

	/**
	 * Insere a mensagem no logger.
	 *
	 * @param loggerClass
	 * @param code
	 */
	protected void logger(Class<?> loggerClass, MessageCode code) {
		logger(loggerClass, Message.get(code));
	}

	/**
	 * Insere a mensagem no logger.
	 *
	 * @param loggerClass
	 * @param message
	 * @param cause
	 */
	protected void logger(Class<?> loggerClass, String message, Throwable cause) {
		Logger logger = LoggerFactory.getLogger(loggerClass);
		logger.error(message, cause);
	}
}
