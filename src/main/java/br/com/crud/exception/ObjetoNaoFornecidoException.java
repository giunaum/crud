package br.com.crud.exception;

import br.com.crud.util.MessageCode;

import java.util.List;

public class ObjetoNaoFornecidoException extends CustomException {

	/**
	 * Construtor da classe de exceção.
	 *
	 * @param message
	 * @param cause
	 */
	public ObjetoNaoFornecidoException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Construtor da classe de exceção.
	 *
	 * @param loggerClass
	 * @param message
	 * @param cause
	 */
	public ObjetoNaoFornecidoException(Class<?> loggerClass, String message, Throwable cause) {
		super(loggerClass, message, cause);
	}

	/**
	 * Construtor da classe de exceção.
	 *
	 * @param message
	 */
	public ObjetoNaoFornecidoException(String message) {
		super(message);
	}

	/**
	 * Construtor da classe de exceção.
	 *
	 * @param loggerClass
	 * @param message
	 */
	public ObjetoNaoFornecidoException(Class<?> loggerClass, String message) {
		super(loggerClass, message);
	}

	/**
	 * Construtor da classe de exceção.
	 *
	 * @param code
	 */
	public ObjetoNaoFornecidoException(MessageCode code) {
		super(code);
	}

	/**
	 * Construtor da classe de exceção.
	 *
	 * @param loggerClass
	 * @param code
	 */
	public ObjetoNaoFornecidoException(Class<?> loggerClass, MessageCode code) {
		super(loggerClass, code);
	}

	/**
	 * Construtor da classe de exceção.
	 *
	 * @param message
	 * @param parametros
	 */
	public ObjetoNaoFornecidoException(String message, List<String> parametros) {
		super(message, parametros);
	}

	/**
	 * Construtor da classe de exceção.
	 *
	 * @param loggerClass
	 * @param message
	 * @param parametros
	 */
	public ObjetoNaoFornecidoException(Class<?> loggerClass, String message, List<String> parametros) {
		super(loggerClass, message, parametros);
	}

	/**
	 * Construtor da classe de exceção.
	 *
	 * @param message
	 * @param parametros
	 */
	public ObjetoNaoFornecidoException(String message, String... parametros) {
		super(message, parametros);
	}

	/**
	 * Construtor da classe de exceção.
	 *
	 * @param loggerClass
	 * @param message
	 * @param parametros
	 */
	public ObjetoNaoFornecidoException(Class<?> loggerClass, String message, String... parametros) {
		super(loggerClass, message, parametros);
	}

	/**
	 * Construtor da classe de exceção.
	 *
	 * @param code
	 * @param parametros
	 */
	public ObjetoNaoFornecidoException(MessageCode code, List<String> parametros) {
		super(code, parametros);
	}

	/**
	 * Construtor da classe de exceção.
	 *
	 * @param loggerClass
	 * @param code
	 * @param parametros
	 */
	public ObjetoNaoFornecidoException(Class<?> loggerClass, MessageCode code, List<String> parametros) {
		super(loggerClass, code, parametros);
	}

	/**
	 * Construtor da classe de exceção.
	 *
	 * @param code
	 * @param parametros
	 */
	public ObjetoNaoFornecidoException(MessageCode code, String... parametros) {
		super(code, parametros);
	}

	/**
	 * Construtor da classe de exceção.
	 *
	 * @param loggerClass
	 * @param code
	 * @param parametros
	 */
	public ObjetoNaoFornecidoException(Class<?> loggerClass, MessageCode code, String... parametros) {
		super(loggerClass, code, parametros);
	}
}
