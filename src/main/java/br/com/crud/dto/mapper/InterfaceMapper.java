package br.com.crud.dto.mapper;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.data.domain.Page;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public interface InterfaceMapper<T, D> {

	T toEntity(D dto);

	D toDTO(T entity);

	default List<D> toListDTO(List<T> entities) {
		return CollectionUtils.isNotEmpty(entities)
				? entities.stream().filter(Objects::nonNull).map(this::toDTO).collect(Collectors.toList())
				: Collections.emptyList();
	}

	default List<T> toListEntity(List<D> dtos) {
		return CollectionUtils.isNotEmpty(dtos)
				? dtos.stream().filter(Objects::nonNull).map(this::toEntity).collect(Collectors.toList())
				: Collections.emptyList();
	}

	default Page<D> toPageDTO(Page<T> dtos) {
		return dtos != null && !dtos.isEmpty() ? dtos.map(this::toDTO) : Page.empty();
	}
}
