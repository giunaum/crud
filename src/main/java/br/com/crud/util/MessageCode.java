package br.com.crud.util;

/**
 * Enum com os código de exceções/mensagens.
 */
public enum MessageCode {

	REGISTRO_NAO_ENCONTRADO("MSG001"),
	PARAMETROS_NAO_FORNECIDOS("MSG002"),
	FALHA_SALVAR_CLIENTE("MSG003"),
	CLIENTE_JA_EXISTE("MSG004"),
	CLIENTE_NAO_EXISTE("MSG005");


	private final String chave;

	/**
	 * Construtor do Enum.
	 *
	 * @param chave
	 */
	MessageCode(String chave) {
		this.chave = chave;
	}

	@Override
	public String toString() {
		return chave;
	}
}
