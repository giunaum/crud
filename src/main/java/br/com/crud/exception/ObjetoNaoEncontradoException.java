package br.com.crud.exception;

import br.com.crud.util.MessageCode;

import java.util.List;

public class ObjetoNaoEncontradoException extends CustomException {

	/**
	 * Construtor da classe de exceção.
	 *
	 * @param message
	 * @param cause
	 */
	public ObjetoNaoEncontradoException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Construtor da classe de exceção.
	 *
	 * @param loggerClass
	 * @param message
	 * @param cause
	 */
	public ObjetoNaoEncontradoException(Class<?> loggerClass, String message, Throwable cause) {
		super(loggerClass, message, cause);
	}

	/**
	 * Construtor da classe de exceção.
	 *
	 * @param message
	 */
	public ObjetoNaoEncontradoException(String message) {
		super(message);
	}

	/**
	 * Construtor da classe de exceção.
	 *
	 * @param loggerClass
	 * @param message
	 */
	public ObjetoNaoEncontradoException(Class<?> loggerClass, String message) {
		super(loggerClass, message);
	}

	/**
	 * Construtor da classe de exceção.
	 *
	 * @param code
	 */
	public ObjetoNaoEncontradoException(MessageCode code) {
		super(code);
	}

	/**
	 * Construtor da classe de exceção.
	 *
	 * @param loggerClass
	 * @param code
	 */
	public ObjetoNaoEncontradoException(Class<?> loggerClass, MessageCode code) {
		super(loggerClass, code);
	}

	/**
	 * Construtor da classe de exceção.
	 *
	 * @param message
	 * @param parametros
	 */
	public ObjetoNaoEncontradoException(String message, List<String> parametros) {
		super(message, parametros);
	}

	/**
	 * Construtor da classe de exceção.
	 *
	 * @param loggerClass
	 * @param message
	 * @param parametros
	 */
	public ObjetoNaoEncontradoException(Class<?> loggerClass, String message, List<String> parametros) {
		super(loggerClass, message, parametros);
	}

	/**
	 * Construtor da classe de exceção.
	 *
	 * @param message
	 * @param parametros
	 */
	public ObjetoNaoEncontradoException(String message, String... parametros) {
		super(message, parametros);
	}

	/**
	 * Construtor da classe de exceção.
	 *
	 * @param loggerClass
	 * @param message
	 * @param parametros
	 */
	public ObjetoNaoEncontradoException(Class<?> loggerClass, String message, String... parametros) {
		super(loggerClass, message, parametros);
	}

	/**
	 * Construtor da classe de exceção.
	 *
	 * @param code
	 * @param parametros
	 */
	public ObjetoNaoEncontradoException(MessageCode code, List<String> parametros) {
		super(code, parametros);
	}

	/**
	 * Construtor da classe de exceção.
	 *
	 * @param loggerClass
	 * @param code
	 * @param parametros
	 */
	public ObjetoNaoEncontradoException(Class<?> loggerClass, MessageCode code, List<String> parametros) {
		super(loggerClass, code, parametros);
	}

	/**
	 * Construtor da classe de exceção.
	 *
	 * @param code
	 * @param parametros
	 */
	public ObjetoNaoEncontradoException(MessageCode code, String... parametros) {
		super(code, parametros);
	}

	/**
	 * Construtor da classe de exceção.
	 *
	 * @param loggerClass
	 * @param code
	 * @param parametros
	 */
	public ObjetoNaoEncontradoException(Class<?> loggerClass, MessageCode code, String... parametros) {
		super(loggerClass, code, parametros);
	}
}
