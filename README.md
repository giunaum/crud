# CRUD CLIENTE API

## Problema
Desenvolva um projeto Java com Spring Boot (utilizando qualquer modulo que achar necessário), que possua uma api REST com CRUD de Cliente (id, nome, cpf, dataNascimento). O CRUD deve possuir uma api de GET, POST, DELETE, PATCH e PUT.

A api de GET deve aceitar query strings para pesquisar os clientes por CPF e nome. Também é necessário que nessa api os clientes voltem paginados e que possua um campo por cliente com a idade calculado dele considerando a data de nascimento.

O banco de dados deve estar em uma imagem Docker ou em um sandbox SAAS (banco SQL).

O projeto deve estar em um repositório no BitBucket e na raiz do projeto deve conter um Postman para apreciação da api.

## Tecnologias Utilizadas
- Java 11
- Spring Boot 2.4.1
- Spring Data MongoDB 2.4.1
- Banco noSQL MongoDB
- Swagger 3.0.0

## Configurações
O banco de dados MongoDB está configurado e apontando para um endereço sandbox SAAS.

spring.data.mongodb.uri: mongodb+srv://admin:admin@cluster0.juzu4.mongodb.net/crudapi?retryWrites=true&w=majority

## Testar a API
A API está com o swagger configurado, basta acessar pelo navegador o endereço: *http://localhost:8080/api/swagger-ui/*.

Outra alternativa para testar a API é usando o Insomnia ou Postman.