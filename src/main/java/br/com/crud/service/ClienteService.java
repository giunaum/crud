package br.com.crud.service;

import br.com.crud.document.Cliente;
import br.com.crud.dto.ClienteDTO;
import br.com.crud.dto.ClienteDetailDTO;
import br.com.crud.dto.ClienteUpdateDTO;
import br.com.crud.dto.mapper.ClienteDetailMapper;
import br.com.crud.dto.mapper.ClienteMapper;
import br.com.crud.dto.mapper.ClienteUpdateMapper;
import br.com.crud.exception.ObjetoEncontradoException;
import br.com.crud.exception.ObjetoNaoEncontradoException;
import br.com.crud.exception.ObjetoNaoFornecidoException;
import br.com.crud.exception.ObjetoNaoPersistidoException;
import br.com.crud.repository.ClienteRepository;
import br.com.crud.util.MessageCode;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class ClienteService {

	private static final String CLIENTE = "Cliente";
	private static final String ID_CLIENTE = "ID do Cliente";

	private final ClienteRepository repository;
	private final ClienteMapper mapper;
	private final ClienteDetailMapper detailMapper;
	private final ClienteUpdateMapper updateMapper;

	public Page<ClienteDetailDTO> findAll(Pageable pageable) {
		Page<Cliente> clientes = repository.findAll(pageable);

		if (clientes.isEmpty()) {
			throw new ObjetoNaoEncontradoException(MessageCode.REGISTRO_NAO_ENCONTRADO);
		}

		return detailMapper.toPageDTO(clientes);
	}

	public Page<ClienteDetailDTO> findByNomeOrCpf(String parametro, Pageable pageable) {
		if (StringUtils.isBlank(parametro)) {
			throw new ObjetoNaoFornecidoException(MessageCode.PARAMETROS_NAO_FORNECIDOS, "Nome ou CPF");
		}

		Page<Cliente> clientes = repository.findByNomeOrCpf(parametro, pageable);

		if (clientes.isEmpty()) {
			throw new ObjetoNaoEncontradoException(MessageCode.REGISTRO_NAO_ENCONTRADO);
		}

		return detailMapper.toPageDTO(clientes);
	}

	public ClienteDetailDTO findById(String id) {
		return findClienteById(id).map(detailMapper::toDTO)
				.orElseThrow(() -> new ObjetoNaoEncontradoException(MessageCode.REGISTRO_NAO_ENCONTRADO));
	}

	public ClienteDetailDTO create(ClienteDTO cliente) {
		if (Objects.isNull(cliente)) {
			throw new ObjetoNaoFornecidoException(MessageCode.PARAMETROS_NAO_FORNECIDOS, CLIENTE);
		}

		verificarSeClienteJaExiste(cliente.getId());

		return Optional.of(cliente).map(mapper::toEntity).map(repository::save).map(detailMapper::toDTO)
				.orElseThrow(() -> new ObjetoNaoPersistidoException(MessageCode.FALHA_SALVAR_CLIENTE));
	}

	public ClienteDetailDTO update(ClienteDTO cliente) {
		if (Objects.isNull(cliente)) {
			throw new ObjetoNaoFornecidoException(MessageCode.PARAMETROS_NAO_FORNECIDOS, CLIENTE);
		}

		verificarSeClienteNaoExiste(cliente.getId());

		return Optional.of(cliente).map(mapper::toEntity).map(repository::save).map(detailMapper::toDTO)
				.orElseThrow(() -> new ObjetoNaoPersistidoException(MessageCode.FALHA_SALVAR_CLIENTE));
	}

	public ClienteDetailDTO update(ClienteUpdateDTO cliente) {
		if (Objects.isNull(cliente)) {
			throw new ObjetoNaoFornecidoException(MessageCode.PARAMETROS_NAO_FORNECIDOS, CLIENTE);
		}

		verificarSeClienteNaoExiste(cliente.getId());

		Optional<Cliente> clienteOptional = repository.findById(cliente.getId())
				.map(c -> updateMapper.toCliente(c, cliente));

		return clienteOptional.map(repository::save).map(detailMapper::toDTO)
				.orElseThrow(() -> new ObjetoNaoPersistidoException(MessageCode.FALHA_SALVAR_CLIENTE));
	}

	public void delete(String id) {
		verificarSeClienteNaoExiste(id);
		repository.delete(detailMapper.toEntity(findById(id)));
	}

	private Optional<Cliente> findClienteById(String id) {
		if (StringUtils.isBlank(id)) {
			throw new ObjetoNaoFornecidoException(MessageCode.PARAMETROS_NAO_FORNECIDOS, ID_CLIENTE);
		}

		return repository.findById(id);
	}

	private void verificarSeClienteJaExiste(String id) {
		if (StringUtils.isNotBlank(id) && findClienteById(id).isPresent()) {
			throw new ObjetoEncontradoException(MessageCode.CLIENTE_JA_EXISTE);
		}
	}

	private void verificarSeClienteNaoExiste(String id) {
		if (findClienteById(id).isEmpty()) {
			throw new ObjetoNaoEncontradoException(MessageCode.CLIENTE_NAO_EXISTE);
		}
	}
}
