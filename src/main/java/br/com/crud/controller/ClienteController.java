package br.com.crud.controller;

import br.com.crud.dto.ClienteDTO;
import br.com.crud.dto.ClienteDetailDTO;
import br.com.crud.dto.ClienteUpdateDTO;
import br.com.crud.service.ClienteService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequiredArgsConstructor
@RestController
@RequestMapping(path = "/cliente")
@Api(value = "Api Rest Cliente")
public class ClienteController {

	private final ClienteService service;

	@GetMapping
	@ApiOperation(value = "Recuperar os Clientes")
	public ResponseEntity<Page<ClienteDetailDTO>> getClientes(@PageableDefault Pageable pageable) {
		return new ResponseEntity<>(service.findAll(pageable), HttpStatus.OK);
	}

	@GetMapping(path = "/filtro/{parametro}")
	@ApiOperation(value = "Recuperar os Clientes Por Nome ou CPF")
	public ResponseEntity<Page<ClienteDetailDTO>> getClienteByNomeOrCpf(@PathVariable String parametro,
																		@PageableDefault Pageable pageable) {
		return new ResponseEntity<>(service.findByNomeOrCpf(parametro, pageable), HttpStatus.OK);
	}

	@GetMapping(path = "/{id}")
	@ApiOperation(value = "Recuperar um Cliente Por ID")
	public ResponseEntity<ClienteDetailDTO> getClienteById(@PathVariable String id) {
		return new ResponseEntity<>(service.findById(id), HttpStatus.OK);
	}

	@PostMapping
	@ApiOperation(value = "Cadastrar um Cliente")
	public ResponseEntity<ClienteDetailDTO> saveCliente(@RequestBody ClienteDTO cliente) {
		return new ResponseEntity<>(service.create(cliente), HttpStatus.CREATED);
	}

	@PutMapping
	@ApiOperation(value = "Atualizar um Cliente")
	public ResponseEntity<ClienteDetailDTO> updateCliente(@RequestBody ClienteDTO cliente) {
		return new ResponseEntity<>(service.update(cliente), HttpStatus.OK);
	}

	@PatchMapping
	@ApiOperation(value = "Atualizar Algus Dados de um Cliente")
	public ResponseEntity<ClienteDetailDTO> updateCliente(@RequestBody ClienteUpdateDTO cliente) {
		return new ResponseEntity<>(service.update(cliente), HttpStatus.OK);
	}

	@DeleteMapping(path = "/{id}")
	@ApiOperation(value = "Excluir um Cliente")
	public ResponseEntity<Void> deleteCliente(@PathVariable String id) {
		service.delete(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}
}
