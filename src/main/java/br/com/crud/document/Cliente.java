package br.com.crud.document;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDate;

@Data
@Builder
@Document
public class Cliente {

	@Id
	private String id;

	private String nome;
	private String cpf;
	private LocalDate dataNascimento;
}
